package Soal01.BangunDatar;

public class Segitiga {
    public double alas;
    public double tinggi;
    public double hasil;

    public Segitiga(){}

    public Segitiga(double paramAlas, double paramTinggi){
        this.alas = paramAlas;
        this.tinggi = paramTinggi;
    }

    public double luas(){
        hasil = alas * tinggi / 2;
        return hasil;
    }

    public double luas(double paramAlas, double paramTinggi){
        hasil = paramAlas * paramTinggi / 2;
        return hasil;
    }
}
