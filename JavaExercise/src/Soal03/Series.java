package Soal03;

import Soal03.FindIndex.Find;

import java.util.Scanner;

public class Series {
    public static void main(String[] args) {
        Find find = new Find();

        Scanner myScan = new Scanner(System.in);

        int[] kumNum = {1, 2, 3, 3, 9, 10, 12, 34, 39, 46, 131, 150, 177};
        int panjangKumNum = kumNum.length;
        int masukanUser;
        Integer nilaiArray;

        System.out.print("Masukkan index array yang anda inginkan : ");
        masukanUser = myScan.nextInt();

        nilaiArray = find.index(panjangKumNum, masukanUser);


        if (nilaiArray == null){
            System.out.println("Format index yang anda masukkan tidak sesuai atau melebihi jumlah index dari array yang ditampung");
        } else {
            System.out.println("Nilai dari index array tersebut adalah : " + kumNum[nilaiArray]);
        }



    }
}
