package com.jonathan.KaryawanApp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class KaryawanAppApplication

fun main(args: Array<String>) {
	runApplication<KaryawanAppApplication>(*args)
}
