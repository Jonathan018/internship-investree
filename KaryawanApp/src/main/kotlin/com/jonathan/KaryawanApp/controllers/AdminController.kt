package com.jonathan.KaryawanApp.controllers


import com.jonathan.KaryawanApp.dtos.LoginDTO
import com.jonathan.KaryawanApp.dtos.Message
import com.jonathan.KaryawanApp.dtos.RegisterDTO
import com.jonathan.KaryawanApp.models.User
import com.jonathan.KaryawanApp.services.UserService
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("admin")
class AdminController(
    private val userService: UserService) {

    @PutMapping("update/{id}")
    fun updateUser(
        @PathVariable("id") id: Int,
        @RequestBody user: User,
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {

        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("Anda belum login"))
            }
            val bodyC = Jwts.parser().setSigningKey("date").parseClaimsJws(jwt).body

            var id_role_sementara = bodyC.issuer.toInt()
            var id_user = bodyC.id.toInt()
            if (id_user != id && id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Bukan milik anda"))
            }
            if (id_user == id) {
                return ResponseEntity(userService.updateUser(id, user), HttpStatus.OK)
            }
            if (id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Anda bukan admin"))
            }
            return ResponseEntity(userService.updateUser(id, user), HttpStatus.OK)

        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("format tidak terisi dengan benar"))
        }


    }
}