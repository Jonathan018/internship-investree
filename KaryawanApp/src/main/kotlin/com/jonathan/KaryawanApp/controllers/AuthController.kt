package com.jonathan.KaryawanApp.controllers

import com.jonathan.KaryawanApp.dtos.LoginDTO
import com.jonathan.KaryawanApp.dtos.Message
import com.jonathan.KaryawanApp.dtos.RegisterDTO
import com.jonathan.KaryawanApp.models.User
import com.jonathan.KaryawanApp.services.UserService
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("auth")
class AuthController(
    private val userService: UserService
) {

    @PutMapping("update/{id}")
    fun updateUser(
        @PathVariable("id") id: Int,
        @RequestBody user: User,
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {


        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("Anda belum login"))
            }
            val bodyC = Jwts.parser().setSigningKey("date").parseClaimsJws(jwt).body

            val id_role_sementara = bodyC.issuer.toInt()
            val id_user = bodyC.id.toInt()
            if (id_user != id && id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Bukan milik anda"))
            }
            if (id_user == id) {
                return ResponseEntity(userService.updateUser(id, user), HttpStatus.OK)
            }
            if (id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Anda bukan admin"))
            }
            return ResponseEntity(userService.updateUser(id, user), HttpStatus.OK)

        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("format tidak terisi dengan benar"))
        }


    }

    @DeleteMapping("delete/{id}")
    fun deleteUser(
        @PathVariable("id") id: Int,
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {


        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("Anda belum login"))
            }
            val bodyC = Jwts.parser().setSigningKey("date").parseClaimsJws(jwt).body

            val id_role_sementara = bodyC.issuer.toInt()
            if (id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Anda bukan admin"))
            }
            return ResponseEntity(userService.deleteUser(id), HttpStatus.OK)

        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("format tidak terisi dengan benar"))
        }


    }

    @PostMapping("register")
    fun register(
        @RequestBody body: RegisterDTO,
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {


        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("Anda belum login"))
            }
            val bodyC = Jwts.parser().setSigningKey("date").parseClaimsJws(jwt).body

            val id_role_sementara = bodyC.issuer.toInt()
            if (id_role_sementara != 1) {
                return ResponseEntity.status(401).body(Message("Anda bukan admin"))
            }

            val user = User()
            user.id_role = body.id_role
            user.username = body.username
            user.password = body.password
            user.nama = body.nama
            user.nohp = body.nohp
            user.gender = body.gender
            user.posisi = body.posisi

            return ResponseEntity.ok(this.userService.save(user))
        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("format tidak terisi dengan benar"))
        }


    }

    @PostMapping("login")
    fun login(
        @RequestBody body: LoginDTO,
        response: HttpServletResponse
    ): ResponseEntity<Any> {
        val user = this.userService.findByUsername(body.username)
            ?: return ResponseEntity.badRequest().body(Message("user not found!"))

        if (!user.comparePassword(body.password)) {
            return ResponseEntity.badRequest().body(Message("invalid password!"))
        }

        val id_user = user.id.toString()
        val id_role_user = user.id_role.toString()

        val jwt = Jwts.builder()
            .setId(id_user)
            .setIssuer(id_role_user)
            .setExpiration(Date(System.currentTimeMillis() + 60 * 24 * 1000))
            .signWith(SignatureAlgorithm.HS256, "date").compact()

        val cookie = Cookie("jwt", jwt)
        cookie.isHttpOnly = true

        response.addCookie(cookie)

        return ResponseEntity.ok(Message("success"))

    }


    @GetMapping("user")
    fun user(
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {

        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("unauthenticated"))
            }
            val body = Jwts.parser().setSigningKey("date").parseClaimsJws(jwt).body

            return ResponseEntity(userService.getById(body.issuer.toInt()), HttpStatus.OK)
        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("unauthenticated"))
        }
    }

    @PostMapping("/logout")
    fun logout(
        response: HttpServletResponse
    ): ResponseEntity<Any> {
        val cookie = Cookie("jwt", "")
        cookie.maxAge = 0

        response.addCookie(cookie)

        return ResponseEntity(Message("Succes Logout"), HttpStatus.OK)
    }
}