package com.jonathan.KaryawanApp.controllers

import com.jonathan.KaryawanApp.models.User
import com.jonathan.KaryawanApp.services.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("user")
class UserController(
    private val userService: UserService
) {

    @GetMapping("AllUser")
    fun getAllUser(): ResponseEntity<List<User>> {
        return ResponseEntity(userService.getuUser(2), HttpStatus.OK)
    }

    @GetMapping("User/{id}")
    fun getUserById(
        @PathVariable("id") id: Int
    ): ResponseEntity<User> {
        return ResponseEntity(userService.getById(id), HttpStatus.OK)
    }

    @GetMapping("Search/{nama}")
    fun search(
        @PathVariable("nama") nama: String
    ): ResponseEntity<List<User>>{
        return ResponseEntity(userService.getSearch(nama), HttpStatus.OK)
    }
}