package com.jonathan.KaryawanApp.repositories

import com.jonathan.KaryawanApp.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import javax.websocket.server.PathParam

interface UserRepository : JpaRepository<User, Int> {
    fun findByUsername(username: String): User?

    @Query("SELECT n FROM User n WHERE n.id_role LIKE %:id_role%")
    fun findByIdRole(@PathParam("id_role") id_role: Int): List<User>?

    @Query("SELECT n FROM User n WHERE n.nama LIKE %:nama%")
    fun findBySearch(@PathParam("nama") nama: String): List<User>?
}