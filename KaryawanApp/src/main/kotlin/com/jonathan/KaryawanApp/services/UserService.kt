package com.jonathan.KaryawanApp.services

import com.jonathan.KaryawanApp.models.User
import com.jonathan.KaryawanApp.repositories.UserRepository
import org.hibernate.sql.Update
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Service

@Service
class UserService(
    private val userRepository: UserRepository
) {

    fun getuUser(id_role: Int) : List<User>? {
        return this.userRepository.findByIdRole(id_role)
    }

    fun getSearch(nama: String) : List<User>? {
        return this.userRepository.findBySearch(nama)
    }

    fun save(user: User): User {
        return this.userRepository.save(user)
    }

    fun findByUsername(username: String): User? {
        return this.userRepository.findByUsername(username)
    }

    fun getById(id: Int): User {
        return this.userRepository.getById(id)
    }

    fun deleteUser(id: Int): Int {
        val user = userRepository.findById(id).orElse(null) ?: return id
        userRepository.deleteById(id)
        return id
    }

    fun updateUser(id: Int, user: User): User{
        val updatedUser = userRepository.findById(id).get()
        updatedUser.id_role = user.id_role
        updatedUser.username = user.username
        user.password = user.password
        updatedUser.nohp = user.nohp
        updatedUser.nama = user.nama
        updatedUser.gender = user.gender
        updatedUser.posisi = user.posisi

        return userRepository.save(updatedUser)
    }
}