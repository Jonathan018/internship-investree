package enums

enum class PlayMusicMenu(val id: String, val desc: String) {
    FINDALLMUSIC("1","Menunjukkan seluruh daftar lagu"),
    SEARCHMUSIC("2","Menampilkan informasi lagu yang dicari"),
    BACK("0", "Untuk kembali ke menu sebelumnya")
}


