package service

import database.MusicDatabase
import database.MusicDatabaseImpl
import database.UserDatabase
import database.UserDatabaseImpl
import model.MusicData
import model.UserData

class MusicServiceImpl : MusicService {
//
//    private val database: UserDatabase = UserDatabaseImpl()
//    private var loggedInUser: UserData? = null
//
//    override fun doLogin(username: String, password: String): UserData? {
//        if (username.isEmpty() || password.isEmpty()) {
//            println("username dan password harus diisi")
//            return null
//        }
//
//        val userData: UserData? = database.findUser(username)
//        if (userData == null) {
//            println("user tidak ditemukan")
//            return null
//        }
//        if (!userData.password.equals(password, false)) {
//            println("password tidak sama")
//            return null
//        }
//
//        println("user berhasil login")
//        loggedInUser = userData
//        return userData
//
//    }
//
//    override fun getLoggedInUser(): UserData? = loggedInUser
//
//    override fun doRegister() {
//        TODO("Not yet implemented")
//    }
//}


    private val database: MusicDatabase = MusicDatabaseImpl()
    private var searchMusic: MusicData? = null
    private var playedMusic: MusicData? = null



    override fun getAllMusic(): List<MusicData> {
        val musicData: List<MusicData>? = database.findAllMusic()
        if (musicData == null) {
            println("Tidak ada lagu pada penyimpanan ini")
            return emptyList()
        }
        println("Judul lagu yang tersedia :")
        println("================================")
        return musicData
    }


    override fun getMusic(title: String): MusicData? {
        if (title.isEmpty()) {
            println("Anda belum memasukkan title yang ingin dicari")
            return null
        }

        val musicData: MusicData? = database.findMusic(title)
        if (musicData == null) {
            println("Judul lagu tidak ditemukan")
            return null
        }

        println("Lagu berhasil ditemukan")
        searchMusic = musicData
        return musicData
    }

    override fun doPlayMusic(id : Int): MusicData? {
        if (id == null) {
            println("anda belum memasukkan id")
            return null
        }

        val musicData: MusicData? = database.findMusic(id)
        if (musicData == null) {
            println("lagu tidak ditemukan")
            return null
        }
        println("Lagu Berhasil Ditemukan")
        playedMusic = musicData
        return musicData
    }


    /*override fun getLoggedInUser(): UserData? = loggedInUser*/

    override fun getPlayedMusic(): MusicData? = playedMusic


    override fun doAddMusic() {
        TODO("Not yet implemented")
    }
}