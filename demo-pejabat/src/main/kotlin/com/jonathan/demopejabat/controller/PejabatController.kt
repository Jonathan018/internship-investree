package com.jonathan.demopejabat.controller

import com.jonathan.demopejabat.entity.Pejabat
import com.jonathan.demopejabat.service.PejabatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import java.awt.PageAttributes.MediaType


@Controller
@RequestMapping(value = ["/pejabat"], produces = [org.springframework.http.MediaType.APPLICATION_JSON_VALUE])
class PejabatController @Autowired constructor(
    private val pejabatService: PejabatService
) {
    @GetMapping
    fun getAllPejabat(): ResponseEntity<List<Pejabat>> {
        return ResponseEntity(pejabatService.getPejabat(), HttpStatus.OK)
    }

    @PostMapping
    fun addNewPejabat(
        @RequestBody pejabat: Pejabat
    ): ResponseEntity<Pejabat> {
        return ResponseEntity(pejabatService.addNewPejabat(pejabat), HttpStatus.OK)
    }

    @PutMapping("{id}")
    fun updatePejabat(
        @PathVariable("id") id: Int,
        @RequestBody pejabat: Pejabat
    ): ResponseEntity<Pejabat> {
        return ResponseEntity(pejabatService.updatePejabat(id,pejabat), HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    fun deletePejabat(
        @PathVariable("id") id:Int
    ): ResponseEntity<Int>{
        return ResponseEntity(pejabatService.deletePejabat(id), HttpStatus.OK)
    }
}