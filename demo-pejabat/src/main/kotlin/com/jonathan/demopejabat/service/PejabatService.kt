package com.jonathan.demopejabat.service

import com.jonathan.demopejabat.entity.Pejabat
import com.jonathan.demopejabat.repository.PejabatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PejabatService @Autowired constructor(
    private val pejabatRepository: PejabatRepository
) {
    fun getPejabat(): List<Pejabat> {
        return pejabatRepository.findAll()
    }

    fun addNewPejabat(pejabat: Pejabat): Pejabat {
        return pejabatRepository.save(pejabat)
    }

    fun updatePejabat(id: Int, pejabat: Pejabat): Pejabat {
        val updatedPejabat = pejabatRepository.findById(id).get()
        updatedPejabat.nama = pejabat.nama
        updatedPejabat.jeniskelamin = pejabat.jeniskelamin
        updatedPejabat.umur = pejabat.umur
        updatedPejabat.pendidikan = pejabat.pendidikan

        return pejabatRepository.save(updatedPejabat)
    }

    fun deletePejabat(id: Int): Int {
        val pejabat = pejabatRepository.findById(id).orElse(null) ?: return id
        pejabatRepository.deleteById(id)
        return id
    }
}