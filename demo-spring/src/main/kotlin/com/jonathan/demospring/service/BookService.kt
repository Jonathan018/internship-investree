package com.jonathan.demospring.service

import com.jonathan.demospring.entity.Book
import com.jonathan.demospring.repository.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@Service
class BookService @Autowired constructor(
    private val bookRepository: BookRepository
) {
    fun getBooks(): List<Book> {
        return bookRepository.findAll()
    }

    fun addNewBook(book: Book): Book {
        return bookRepository.save(book)
    }

    fun updateBook(id: Int, book: Book) : Book {
        val updatedBook = bookRepository.findById(id).get()
        updatedBook.title = book.title
        updatedBook.isbn = book.isbn
        updatedBook.author = book.author
        updatedBook.year = book.year

        return bookRepository.save(updatedBook)
    }

    fun deleteBook(id : Int) : Int {
        val book = bookRepository.findById(id).orElse(null) ?: return id
        bookRepository.deleteById(id)
        return id
    }
}